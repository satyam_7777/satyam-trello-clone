const boardContainer=document.querySelector(".board-container");
const key="6ec072eef050ec7be8981f0cc1fb128e";
const token="fae1ce16db4e108a3681db672b88daf1beade1d7b1bb7ee1e84e20b254de2843";

function fetchBoards(){

    fetch(`https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json'
        }
      })
        .then(response => {
          console.log(
            `Response: ${response.status} ${response.statusText}`
          );
          return response.json();
        })
        .then(boardArray => addBoard(boardArray))
        .catch(err => console.error(err));
}

function addBoard(boardArray)
{
  boardArray.forEach((singleBoard)=> {
    board(singleBoard);
  });
  
}

function board(singleBoard){
  const board=document.createElement("div");
    board.classList.add("board");
    const delBtn=document.createElement("button");
    delBtn.classList.add("delete");
    delBtn.innerText="X";
   board.setAttribute("id",singleBoard.id)
   const boardName=document.createElement("div");
   boardName.classList.add("boardName")
   boardName.innerText=singleBoard.name;
    
    board.style.backgroundColor =singleBoard.prefs.background;
    delBtn.style.backgroundColor =singleBoard.prefs.background;
        board.addEventListener("click", (e) => {
          if(e.target.classList.contains("delete")){
            delBoard(singleBoard.id);
           
            
          } else {

            location.href = `/list.html?id=${singleBoard.id}`;
          }
        })
        board.appendChild(boardName);
        board.appendChild(delBtn);
       
        boardContainer.insertAdjacentElement("afterbegin", board);


// document.querySelector(".delete").addEventListener("click",()=>{
//   const boardId=document.querySelector(`${singleBoard.id}`)
//   delBoard(boardId);
// })
}

fetchBoards();

function createBoard(boardName)
{

fetch(`https://api.trello.com/1/boards/?key=${key}&token=${token}&name=${boardName}`, {
  method: 'POST'
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(newBoard => {
    board(newBoard);
    
  })
  .catch(err => console.error(err));
}

document.querySelector(".create-board").addEventListener("click", () => {
  document.querySelector(".popup-container").style.display = "flex";
})




document.querySelector(".popup").addEventListener("click", (event) => {
  if(event.target.classList.contains("cancel")){

    document.querySelector(".popup-container").style.display = "none";
    const boardName = event.target.parentNode.previousElementSibling;
    boardName.value = null;

  } else if(event.target.classList.contains("create")){
    const boardName = event.target.parentNode.previousElementSibling;
    createBoard(boardName.value);
    boardName.value = null;
    document.querySelector(".popup-container").style.display = "none";
  }
})

function delBoard(boardId)
{
   
fetch(`https://api.trello.com/1/boards/${boardId}?key=${key}&token=${token}`, {
  method: 'DELETE'
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(text => document.getElementById(`${boardId}`).remove())
  .catch(err => console.error(err));
}
